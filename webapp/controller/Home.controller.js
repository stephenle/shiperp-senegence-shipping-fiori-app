sap.ui.define([
	"com/senegence/fioriapps/shipping/controller/BaseController",
	"sap/ui/model/json/JSONModel",
	"com/senegence/fioriapps/shipping/model/formatter",
	"sap/m/MessageToast",
	"sap/m/MessageBox"
], function(BaseController, JSONModel, formatter, MessageToast, MessageBox) {
	"use strict";

	return BaseController.extend("com.senegence.fioriapps.shipping.controller.Home", {

		//Link to "com.senegence.fioriapps.shipping.model.formatter" for formatter functions
		formatter: formatter,

		/* =========================================================== */
		/* Lifecycle methods                                           */
		/* =========================================================== */

		/**
		 * Called when the worklist controller is instantiated.
		 * @public
		 */
		onInit: function() {
			//1. When ever user go to the route "home", trigger this._onObjectMatched()
			this.getRouter().getRoute("home").attachPatternMatched(this._onObjectMatched, this);
		},

		_onObjectMatched: function() {
			var oShip_station = this.getView().byId("ship_station");
			$("#" + oShip_station.sId + "-inner").attr("autocomplete", "off");
			oShip_station.onAfterRendering = function() {
				if (sap.m.Input.onAfterRendering) {
					sap.m.Input.onAfterRendering.apply(this, arguments);
				}
				$("#" + oShip_station.sId + "-inner").attr("autocomplete", "off");
			};

		},

		/* =========================================================== */
		/* Event handlers                                              */
		/* =========================================================== */
		onLogin: function() {

			var oModel = this.getModel();
			var sFunction = "/Login";
			var sShip_Station = this.getView().byId("ship_station").getValue();
			if (sShip_Station === "") {
				MessageBox.error("Please enter Shipping Station");
			} else {
				this.showBusy();
				oModel.callFunction(sFunction, {
					method: "POST",
					urlParameters: {
						'emp_no': sShip_Station
					},
					success: function(oData) {
						this.hideBusy();
						if (oData.Data1 === "AAAAAAAAAAE=") {

							if (oData.Data3 === "AAAAAAAAAAE=") {
								this.getModel("userAutho").setProperty("/autho", true);
							} else if (oData.Data2 === "AAAAAAAAAAE=") {
								this.getModel("userAutho").setProperty("/autho", false);
							}
							this.getRouter().navTo("main", {
								shipstation: sShip_Station
							});
						} else {
							MessageToast.show("Invalid Ship Station Value");
						}

					}.bind(this),
					error: function(oError) {
						this.hideBusy();
						this._handleODataError(oError);
					}.bind(this)
				});
			}

		},

		onSubmit: function(oEvent) {
			var oModel = this.getModel();
			var sFunction = "/Login";
			var sShip_Station = this.getView().byId("ship_station").getValue();
			if (sShip_Station === "") {
				MessageBox.error("Please enter Shipping Station");
			} else {
				this.showBusy();
				oModel.callFunction(sFunction, {
					method: "POST",
					urlParameters: {
						'emp_no': sShip_Station
					},
					success: function(oData) {
						this.hideBusy();
						if (oData.Data1 === "AAAAAAAAAAE=") {

							if (oData.Data3 === "AAAAAAAAAAE=") {
								this.getModel("userAutho").setProperty("/autho", true);
							} else if (oData.Data2 === "AAAAAAAAAAE=") {
								this.getModel("userAutho").setProperty("/autho", false);
							}
							this.getRouter().navTo("main", {
								shipstation: sShip_Station
							});
						} else {
							MessageToast.show("Invalid Ship Station Value");
						}

					}.bind(this),
					error: function(oError) {
						this.hideBusy();
						this._handleODataError(oError);
					}.bind(this)
				});
			}
		}

		/* =========================================================== */
		/* Internal methods                                            */
		/* =========================================================== */

	});
});