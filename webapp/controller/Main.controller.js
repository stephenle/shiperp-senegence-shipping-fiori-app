/*global location*/
sap.ui.define([
	"com/senegence/fioriapps/shipping/controller/BaseController",
	"sap/ui/model/json/JSONModel",
	"com/senegence/fioriapps/shipping/model/formatter",
	"sap/m/MessageBox",
	"sap/m/MessageToast",
	'sap/ui/model/FilterOperator',
	'sap/ui/model/FilterType'
], function(BaseController, JSONModel, formatter, MessageBox, MessageToast, FilterOperator, FilterType) {
	"use strict";

	return BaseController.extend("com.senegence.fioriapps.shipping.controller.Main", {

		formatter: formatter,

		/* =========================================================== */
		/* lifecycle methods                                           */
		/* =========================================================== */

		/**
		 * Called when the worklist controller is instantiated.
		 * @public
		 */
		onInit: function() {
			// Model used to manipulate control states. The chosen values make sure,
			// detail page is busy indication immediately so there is no break in
			// between the busy indication for loading the view's meta data
			var oViewModel = new JSONModel({});
			this.getRouter().getRoute("main").attachPatternMatched(this._onObjectMatched, this);

			this.setModel(oViewModel, "mainView");

			// Initialize Message Model
			var oJSONModel = new JSONModel({
				aMessages: [],
				messagesLength: 0
			});
			this.setModel(oJSONModel, "messageModel");
		},

		/* =========================================================== */
		/* event handlers                                              */
		/* =========================================================== */
		onEditAddress: function() {
			var totBarcode = this.getView().byId("totBarcode").getValue();
			var sPath = "/ShipDataSet('" + totBarcode + "')" + "/ShipFromAddr";

			if (!this.oAddrDialog) {
				this.oAddrDialog = sap.ui.xmlfragment("com.senegence.fioriapps.shipping.fragment.AddressDialog", this);
				this.getView().addDependent(this.oAddrDialog);
			}

			this.oAddrDialog.bindElement({
				path: sPath,
				events: {
					change: this._onBindingChange.bind(this),
					dataRequested: function() {
						this.showBusy();
					}.bind(this),
					dataReceived: function() {
						this.hideBusy();
					}.bind(this)
				}
			});
			this.oAddrDialog.open();
		},

		onCloseDialog: function() {
			this.oAddrDialog.close();
		},

		onSubmitDialog: function() {
			this.oAddrDialog.close();
		},

		onTotBarcodeSubmit: function(oEvent) {
			var sShipID = oEvent.getSource().getValue();
			var sPath = "/ShipDataSet('" + sShipID + "')";
			var oModel = this.getModel();
			$("div .totBarcode input").select();
			this.getView().byId("erpCarr").setEnabled(false);
			this.getView().byId("carrServ").setEnabled(false);
			this.getView().byId("erpPackm").setEnabled(false);
			if (sShipID === "") {
				MessageBox.error("Invalid TOTE Barcode");
			} else {
				this.showBusy();
				if (this.getView().getBindingContext() === undefined) {
					oModel.read(sPath, {
						urlParameters: {
							"$expand": "HUData,ShipTo,ShipOutput"
						},
						success: function(oData) {
							this.getModel("mainView").setProperty("/shipData", oData);
							this.getModel("mainView").setProperty("/HUData", oData.HUData.results);
							this.getModel("mainView").setProperty("/shipTo", oData.ShipTo);
							this.getModel("mainView").setProperty("/totalItems", oData.HUData.results.length);
							var stotalItem = 0;
							var sCarrierFlg = false;
							var sCarrierSerFlg = false;
							var sPackMatFlg = false;
							for (var i = 0; i < oData.HUData.results.length; i++) {
								stotalItem = stotalItem + parseFloat(oData.HUData.results[i].Quan);
							}
							this.getModel("mainView").setProperty("/totalItems", stotalItem);

							var oShipOutput = {
								shippinglable: false,
								nafta: false,
								usfta: false,
								packingslip: false,
								commercialinvoice: false
							};

							oShipOutput.shippinglable = (oData.ShipOutput.Data1 !== "" ? true : false);
							oShipOutput.nafta = (oData.ShipOutput.Data2 !== "" ? true : false);
							oShipOutput.usfta = (oData.ShipOutput.Data3 !== "" ? true : false);
							oShipOutput.packingslip = (oData.ShipOutput.Data4 !== "" ? true : false);
							oShipOutput.commercialinvoice = (oData.ShipOutput.Data5 !== "" ? true : false);

							this.getView().getModel("mainView").setProperty("/shipOutput", oShipOutput);

							var sShipStation = this.getView().byId("shipStation").getValue();
							this.bindSelectListCarr(oData.Carrier, sShipStation);
							var oBindingCarr = this.getView().byId("erpCarr").getBinding("items");
							oBindingCarr.attachDataReceived(function(oDataCarr) {
								if (oDataCarr.getSource().getLength() !== 0) {
									sCarrierFlg = true;
									this.getView().byId("erpCarr").setSelectedKey(oData.Carrier);
								}
							}.bind(this));

							this.bindCarrService(oData.Carrier, sShipStation);
							var oBindingServ = this.getView().byId("carrServ").getBinding("items");
							oBindingServ.attachDataReceived(function(oDataServ) {
								if (oDataServ.getSource().getLength() !== 0) {
									sCarrierSerFlg = true;
									this.getView().byId("carrServ").setSelectedKey(oData.Service);
								}
							}.bind(this));

							this.bindPackMat(oData.Carrier);
							var oBindingMat = this.getView().byId("erpPackm").getBinding("items");
							oBindingMat.attachDataReceived(function(oDataMat) {
								if (oDataMat.getSource().getLength() !== 0) {
									sPackMatFlg = true;
									this.getView().byId("erpPackm").setSelectedKey(oData.PackMat);
								}

								if (sCarrierFlg === false || sCarrierSerFlg === false || sPackMatFlg === false) {
									this.getView().byId("btnShip").setEnabled(false);
									this.getView().byId("btnChange").setEnabled(false);
								} else {
									this.getView().byId("btnShip").setEnabled(true);
									this.getView().byId("btnChange").setEnabled(true);
								}
							}.bind(this));

							this.getView().byId("erpPackm").setValueState();
							this.getView().byId("carrServ").setValueState();
							this.getView().byId("totBarcode").setValueState();

							this.hideBusy();
						}.bind(this),
						error: function(oError) {
							var aErroMsg = JSON.parse(oError.responseText).error.innererror.errordetails;
							for (var i = 0; i < aErroMsg.length; i++) {
							if (aErroMsg[i].message !== "") {
								MessageBox.error(aErroMsg[i].message, {
									title: "Error",
									onClose: function() {
										$("div .totBarcode input").select();
									}
								});
								this.hideBusy();
								return;
							}
						}
						}.bind(this)
					});
				} else {
					this.getView().getModel().refresh();
				}
			}
		},

		onUpdateFinished: function() {
			this.hideBusy();
		},

		onCarrChange: function(oEvent) {
			//When carrier change in Carrier Select List.
			//Carrier Service Type and Packing Materi will change accordingly.
			// For Car Service Type
			var Filter = new sap.ui.model.Filter({
				path: "carrier",
				operator: FilterOperator.EQ,
				value1: oEvent.getSource().getSelectedItem().getKey()
			});
			var arrFitler = [];
			arrFitler.push(Filter);

			var sShipStation = this.getView().byId("shipStation").getValue();
			var ShipStation = sShipStation.substring(0, 4);
			var Filter01 = new sap.ui.model.Filter({
				path: "vstel_ref",
				operator: FilterOperator.EQ,
				value1: ShipStation
			});
			arrFitler.push(Filter01);

			var oBinding = this.getView().byId("carrServ").getBinding("items");
			oBinding.filter(arrFitler, FilterType.Application);
			arrFitler = [];

			// For Packking Material
			var matFilter = new sap.ui.model.Filter({
				path: "carrier",
				operator: FilterOperator.EQ,
				value1: oEvent.getSource().getSelectedItem().getKey()
			});
			arrFitler.push(matFilter);
			var oMatBinding = this.getView().byId("erpPackm").getBinding("items");
			oMatBinding.filter(arrFitler, FilterType.Application);
			arrFitler = [];
		},

		onShip: function(oEvent) {
			var oModel = this.getModel();
			var stotBarcode = this.getView().byId("totBarcode").getValue();
			if (this.getView().byId("totBarcode").getValue() === "") {
				MessageBox.error("TOTE Barcode is empty");
				this.getView().byId("totBarcode").setValueState("Error");
			} else if (this.getView().byId("carrServ").getItems().length === 0) {
				MessageBox.error("Carrier Service is empty");
				this.getView().byId("carrServ").setValueState("Error");
			} else if (this.getView().byId("erpPackm").getItems().length === 0) {
				MessageBox.error("Packing Material is empty");
				this.getView().byId("erpPackm").setValueState("Error");
			} else {
				this.getModel("messageModel").setProperty("/aMessages", []);
				var oData = {
					ShipTypeInput: "",
					ShipID: this.getView().byId("totBarcode").getValue(),
					Profile: "",
					ShipStation: this.getView().byId("shipStation").getValue(),
					PackMat: this.getView().byId("erpPackm").getSelectedKey(),
					LGPLA: "",
					Carrier: this.getView().byId("erpCarr").getSelectedKey(),
					Service: this.getView().byId("carrServ").getSelectedKey(),
					HUData: this.getView().getModel("mainView").getProperty("/HUData")
				};
				this.showBusy();
				oModel.create("/ShipDataSet", oData, {
					success: function(oData) {
						this.hideBusy();
						// Clear content and Set focus to TOTE Barcode
						this.clearValue();
						this.getView().byId("totBarcode").setValue(stotBarcode);
						$("div .totBarcode input").select();
						this.getView().byId("btnShip").setEnabled(false);
						this.getView().byId("btnChange").setEnabled(false);
						MessageToast.show("Ship excute successful. Tracking number:" + oData.TrackingNumber);
					}.bind(this),
					error: function(oError) {
						var aErroMsg = JSON.parse(oError.responseText).error.innererror.errordetails;
						for (var i = 0; i < aErroMsg.length; i++) {
							if (aErroMsg[i].message !== "") {
								MessageBox.error(aErroMsg[i].message, {
									title: "Error",
									onClose: function() {
										$("div .totBarcode input").select();
									}
								});
								this.hideBusy();
								return;
							}
						}
					}.bind(this)
				});
			}

		},

		onCancel: function(oEvent) {
			this.clearValue();
			this.getView().byId("btnShip").setEnabled(false);
			this.getView().byId("btnChange").setEnabled(false);
		},

		onChange: function(oEvent) {
			// Based on the User Role, the application allow or nor allow for changing these fields
			// Role for Superivisor ( Enable = true ) - Normal User ( Enable = false )
			if (this.getView().byId("totBarcode").getValue() === "") {
				MessageBox.error("TOTE Barcode is empty");
				this.getView().byId("totBarcode").setValueState("Error");
			} else {
				this.getView().byId("erpCarr").setEnabled(this.getModel("userAutho").getProperty("/autho"));
				this.getView().byId("carrServ").setEnabled(this.getModel("userAutho").getProperty("/autho"));
				this.getView().byId("erpPackm").setEnabled(this.getModel("userAutho").getProperty("/autho"));
			}
		},

		onLogOff: function() {
			this.clearValue();
			this.getRouter().navTo("home");
		},

		_onBindingChange: function() {

		},

		/* =========================================================== */
		/* internal methods                                            */
		/* =========================================================== */
		/**
		 * Binds the view to the object path.
		 * @function
		 * @param {sap.ui.base.Event} oEvent pattern match event in route 'object'
		 * @private
		 */
		_onObjectMatched: function(oEvent) {
			this.getView().byId("shipStation").setValue(oEvent.getParameters().arguments.shipstation);
			this.getView().byId("btnShip").setEnabled(false);
			this.getView().byId("btnChange").setEnabled(false);
		},

		bindSelectListCarr: function(sCarrier, sShipStation) {
			var sCarrPath = "/ZSERP_CARRSERV";
			var arrFilter = [];

			var ShipStation = sShipStation.substring(0, 4);
			var Filter01 = new sap.ui.model.Filter({
				path: "vstel_ref",
				operator: FilterOperator.EQ,
				value1: ShipStation
			});
			arrFilter.push(Filter01);

			this.getView().byId("erpCarr").getModel().setSizeLimit(3000);
			this.getView().byId("erpCarr").bindItems({
				path: sCarrPath,
				filters: arrFilter,
				template: new sap.ui.core.Item({
					key: "{carrier}",
					text: "{description}"
				})
			});

			var oBinding = this.getView().byId("erpCarr").getBinding("items");
			oBinding.attachDataReceived(function() {
				var aItems = this.getView().byId("erpCarr").getItems();
				this.getView().byId("erpCarr").removeAllItems();
				for (var i = 0; i < aItems.length; i++) {
					if (i !== 0) {
						if (aItems[i].getKey() !== aItems[i - 1].getKey()) {
							this.getView().byId("erpCarr").addItem(aItems[i]);
						}
					} else {
						this.getView().byId("erpCarr").addItem(aItems[i]);
					}
				}
			}.bind(this));

		},

		// Load data and binding to Carrier Service Select List
		bindCarrService: function(sCarrier, ShipStation) {
			var sCarrSerPath = "/ZSERP_CARRSERV";
			var arrFitler = [];
			// For Car Service Type
			var oFilter = new sap.ui.model.Filter({
				path: "carrier",
				operator: FilterOperator.EQ,
				value1: sCarrier
			});
			arrFitler.push(oFilter);

			ShipStation = ShipStation.substring(0, 4);
			var oFilter01 = new sap.ui.model.Filter({
				path: "vstel_ref",
				operator: FilterOperator.EQ,
				value1: ShipStation
			});
			arrFitler.push(oFilter01);

			this.getView().byId("carrServ").bindItems({
				path: sCarrSerPath,
				filters: arrFitler,
				template: new sap.ui.core.Item({
					key: "{service}",
					text: "{service} {servicedesc}"
				})
			});
		},

		// Load data and binding to Packing Material Select List
		bindPackMat: function(sCarrier) {
			var sPackMPath = "/ZSERP_PACKM";
			var arrFitler = [];
			var matFilter = new sap.ui.model.Filter({
				path: "carrier",
				operator: FilterOperator.EQ,
				value1: sCarrier
			});
			arrFitler.push(matFilter);
			this.getView().byId("erpPackm").bindItems({
				path: sPackMPath,
				filters: arrFitler,
				template: new sap.ui.core.Item({
					key: "{packmat}",
					text: "{packmat}: {maktx}"
				})
			});
		},

		clearValue: function() {
			this.getView().byId("totBarcode").setValue("");
			this.getView().byId("totBarcode").focus();
			this.getView().byId("totalItems").setValue("");
			this.getModel("mainView").setProperty("/shipOutput", {});
			this.getModel("mainView").setProperty("/shipTo", {});
			this.getModel("mainView").setProperty("/HUData", {});
			this.getModel("mainView").setProperty("/shipData", {});
			this.getView().byId("erpCarr").setSelectedKey(0);
			this.getView().byId("carrServ").setSelectedKey(0);
			this.getView().byId("erpPackm").setSelectedKey(0);
			this.getView().byId("erpCarr").setEnabled(false);
			this.getView().byId("carrServ").setEnabled(false);
			this.getView().byId("erpPackm").setEnabled(false);
			this.getView().byId("erpPackm").setValueState();
			this.getView().byId("carrServ").setValueState();
			this.getView().byId("totBarcode").setValueState();
			this.getView().byId("erpCarr").removeAllItems();
			this.getView().byId("carrServ").removeAllItems();
			this.getView().byId("erpPackm").removeAllItems();
			this.getView().byId("btnShip").setEnabled(false);
			this.getView().byId("btnChange").setEnabled(false);
		}
	});
});