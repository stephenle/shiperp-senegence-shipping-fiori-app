sap.ui.define([], function() {
	"use strict";

	return {

		formatLogoImage: function(sDummy) {
			var sRootPath = jQuery.sap.getModulePath("com.senegence.fioriapps.shipping");
			return sRootPath + "/images/logo.jpg";
		}

	};

});